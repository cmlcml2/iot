import paho.mqtt.client as mqtt
from gpiozero import *
import glob
import time
from time import gmtime, strftime
import os
import RPi.GPIO as gpio


ledBathroom = 13
PIR_MOTION = 4
door = 12

# GPIO to LCD mapping
LCD_RS = 7   # Pi pin 26
LCD_E  = 8   # Pi pin 24
LCD_D4 = 25  # Pi pin 22
LCD_D5 = 24  # Pi pin 18
LCD_D6 = 23  # Pi pin 16
LCD_D7 = 18  # Pi pin 12

# Device constants
LCD_CHR = True    # Character mode
LCD_CMD = False   # Command mode
LCD_CHARS = 16    # Characters per line (16 max)
LCD_LINE_1 = 0x80 # LCD memory location for 1st line
LCD_LINE_2 = 0xC0 # LCD memory location 2nd line

GPIO.setmode(GPIO.BCM)

GPIO.setup(door,GPIO.OUT)
GPIO.setup(ledBathroom,GPIO.OUT)
GPIO.setup(PIR_MOTION, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
GPIO.setup(LCD_E, GPIO.OUT)  # Set GPIO's to output mode
GPIO.setup(LCD_RS, GPIO.OUT)
GPIO.setup(LCD_D4, GPIO.OUT)
GPIO.setup(LCD_D5, GPIO.OUT)
GPIO.setup(LCD_D6, GPIO.OUT)
GPIO.setup(LCD_D7, GPIO.OUT)

servo = gpio.PWM(door,50)
servo.start(2.35)

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*') [0]
device_file = device_folder + '/w1_slave'

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp(bool):
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        if(bool) :
            return str(temp_c) + "C"
        else :
            return str(temp_f) + "F"


# Initialize and clear display
def lcd_init():
  lcd_write(0x33,LCD_CMD) # Initialize
  lcd_write(0x32,LCD_CMD) # Set to 4-bit mode
  lcd_write(0x06,LCD_CMD) # Cursor move direction
  lcd_write(0x0C,LCD_CMD) # Turn cursor off
  lcd_write(0x28,LCD_CMD) # 2 line display
  lcd_write(0x01,LCD_CMD) # Clear display
  time.sleep(0.0005)     # Delay to allow commands to process

def lcd_write(bits, mode):
# High bits
  GPIO.output(LCD_RS, mode) # RS

  GPIO.output(LCD_D4, False)
  GPIO.output(LCD_D5, False)
  GPIO.output(LCD_D6, False)
  GPIO.output(LCD_D7, False)
  if bits&0x10==0x10:
    GPIO.output(LCD_D4, True)
  if bits&0x20==0x20:
    GPIO.output(LCD_D5, True)
  if bits&0x40==0x40:
    GPIO.output(LCD_D6, True)
  if bits&0x80==0x80:
    GPIO.output(LCD_D7, True)

# Toggle 'Enable' pin
  lcd_toggle_enable()

# Low bits
  GPIO.output(LCD_D4, False)
  GPIO.output(LCD_D5, False)
  GPIO.output(LCD_D6, False)
  GPIO.output(LCD_D7, False)
  if bits&0x01==0x01:
    GPIO.output(LCD_D4, True)
  if bits&0x02==0x02:
    GPIO.output(LCD_D5, True)
  if bits&0x04==0x04:
    GPIO.output(LCD_D6, True)
  if bits&0x08==0x08:
    GPIO.output(LCD_D7, True)

# Toggle 'Enable' pin
  lcd_toggle_enable()

def lcd_toggle_enable():
  time.sleep(0.0005)
  GPIO.output(LCD_E, True)
  time.sleep(0.0005)
  GPIO.output(LCD_E, False)
  time.sleep(0.0005)

def lcd_text(message,line):
  # Send text to display
  message = message.ljust(LCD_CHARS," ")

  lcd_write(line, LCD_CMD)

  for i in range(LCD_CHARS):
    lcd_write(ord(message[i]),LCD_CHR)
# Initialize display
lcd_init()

        
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
# Subscribing in on_connect() means that if we lose the connection and reconnect then subscriptions will be renewed.
    client.subscribe("listen")
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    if msg.payload == "onLed":
        GPIO.output(ledBathroom, GPIO.HIGH)
    if msg.payload == "offLed":
        GPIO.output(ledBathroom, GPIO.LOW)
    if msg.payload == "openDoor":
        servo.ChangeDutyCycle(6.7)
    if msg.payload == "closeDoor":
        servo.ChangeDutyCycle(2.35)
    if msg.payload == "getTempFr":
        test = str(read_temp(True))
        datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        lcd_text("Temperature :",LCD_LINE_1)
        lcd_text(test,LCD_LINE_2)

        time.sleep(3) # 3 second delay
        
        lcd_text("Date :",LCD_LINE_1)
        lcd_text(datetime,LCD_LINE_2)
    if msg.payload == "getTempEn":
        test = str(read_temp(False))
        datetime = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        lcd_text("Temperature :",LCD_LINE_1)
        lcd_text(test,LCD_LINE_2)

        time.sleep(3) # 3 second delay
        
        lcd_text("Date :",LCD_LINE_1)
        lcd_text(datetime,LCD_LINE_2)
        
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
# Using the broker hosted at localhost
client.connect("localhost", 1883, 60)
# normally the IP section is filled with "localhost" or "127.0.0.1"
# this assumes that the MQTT broker is the Raspberry Pi itself
# this assumes that the MQTT broker service has already started
# check service status: sudo systemctl status mosquitto.service
#client.loop_forever()
try:
    while True:
        if GPIO.input(PIR_MOTION):
            GPIO.output(ledBathroom, GPIO.HIGH)
        else:
            GPIO.output(ledBathroom, GPIO.LOW)
        time.sleep(10)
            
            
except KeyboardInterrupt:
    GPIO.cleanup()