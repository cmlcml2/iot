from flask import Flask
from flask_cors import CORS
import glob
import os
import time

led = False
servoDoor = False
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*') [0]
device_file = device_folder + '/w1_slave'
os.system("mosquitto_pub -h 127.0.0.1 -t listen -m getTempFr")  

app = Flask(__name__)
CORS(app)

@app.route('/')
def hello():
    return "Hello World!"

@app.route('/LightOn')
def AllLightsOn():
    global led
    
    os.system("mosquitto_pub -h 127.0.0.1 -t listen -m onLed")
    led = True
    return ""

@app.route('/LightOff')
def AllLightsOff():
    global led
    
    os.system("mosquitto_pub -h 127.0.0.1 -t listen -m offLed")
    led = False
    return ""

@app.route('/openDoor')
def WSwitch():
    global servoDoor

    if servoDoor == True : 
        os.system("mosquitto_pub -h 127.0.0.1 -t listen -m openDoor")
        servoDoor = False
        return "Door is opened"
    else :
        os.system("mosquitto_pub -h 127.0.0.1 -t listen -m closeDoor")
        servoDoor = True
        return "Door is closed"

@app.route('/GetTempFr')
def GetTemp():
    os.system("mosquitto_pub -h 127.0.0.1 -t listen -m getTempFr")
    time.sleep(3)
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    
    return "temperature\": \""+str(lines[0])+""
    #return ''



if __name__ == '__main__':
    app.run(host= '0.0.0.0')