import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from './app.service';
import { interval } from 'rxjs';





@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'smarthome';
  temps: string;

  constructor(private translate: TranslateService, public service: AppService) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.getTemp();
    interval(20000).subscribe(x => this.getTemp());
  }



  getTemp(): void {
    if (this.translate.currentLang === 'fr') {
      this.service.getTempFR().subscribe(
        (res) => {
          console.log(res)
          this.temps = res;
        },
        (err) => console.log("Erreur", err)
      );
    } else {
      this.service.getTempEn().subscribe(
        (res) => {
          console.log(res)
          this.temps = res;
        },
        (err) => console.log("Erreur", err)
      );
    }

  }

  useLanguage(language: string) {
    this.translate.use(language);
    this.getTemp();
  }

  openDoor(): void {
    this.service.openDoor(12345).subscribe(
      (res) => {
        console.log(res)
      },
      (err) => console.log("Erreur", err)
    );;
  }

  closeDoor(): void {
    this.service.closeDoor().subscribe(
      (res) => {
        console.log(res)
      },
      (err) => console.log("Erreur", err)
    );;
  }

  switchOnLight() {
    this.service.switchOnLight().subscribe(
      (res) => {
        console.log(res)
      },
      (err) => console.log("Erreur", err)
    );;
  }

  switchOffLight() {
    this.service.switchOffLight().subscribe(
      (res) => {
        console.log(res)
      },
      (err) => console.log("Erreur", err)
    );;
  }
}
