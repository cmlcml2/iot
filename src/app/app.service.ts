import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AppService {
    constructor(private http: HttpClient) { }

    openDoor(code: number): any {
        console.log('open the door')
        return this.http.get('http://10.3.4.6:5000/openDoor', { responseType: 'text' });
    }

    closeDoor(): any {
        console.log('open the door')
        return this.http.get('http://10.3.4.6:5000/closeDoor', { responseType: 'text' });
    }

    getTempFR(): any {
        return this.http.get('http://10.3.4.6:5000/GetTempFr', { responseType: 'text' });
    }

    getTempEn(): any {
        return this.http.get('http://10.3.4.6:5000/GetTempEn', { responseType: 'text' });
    }

    switchOffLight(): any {
        return this.http.get('http://10.3.4.6:5000/LightOff');
    }

    switchOnLight(): any {
        console.log('Light on')
        return this.http.get('http://10.3.4.6:5000/LightOn');
    }
}